DROP TABLE IF EXISTS user;
DROP TABLE IF EXISTS deals;
DROP TABLE IF EXISTS inst;
DROP TABLE IF EXISTS cpty;

CREATE TABLE user
(
    id       INT NOT NULL AUTO_INCREMENT,
    username VARCHAR(45),
    pass     VARCHAR(255),
    PRIMARY KEY (id)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;


create table inst
(
    inst_id   INT         NOT NULL AUTO_INCREMENT,
    inst_name VARCHAR(20) NOT NULL,

    PRIMARY KEY (inst_id)
) ENGINE = INNODB;
INSERT INTO inst (inst_name) VALUES ("Astronomica"),( "Borealis"),("Celestial"), ("Deuteronic"), ("Eclipse"),
            ("Floral"), ("Galactia"), ("Heliosphere"), ("Interstella"), ("Jupiter"), ("Koronis"), ("Lunatic");

CREATE TABLE cpty
(
    cpty_id   INT         NOT NULL AUTO_INCREMENT,
    cpty_name VARCHAR(20) NOT NULL,

    PRIMARY KEY (cpty_id)
) ENGINE = INNODB;
INSERT INTO cpty (cpty_name) VALUES ("Lewis"), ("Selvyn"), ("Richard"), ("Lina"), ("John"), ("Nidia");

CREATE TABLE deals
(
    deal_id   INT NOT NULL AUTO_INCREMENT,
    inst_id   INT,
    cpty_id   INT,
    price     FLOAT(20, 13),
    deal_type CHAR,
    quantity  INT,
    deal_time TIMESTAMP(6),

    PRIMARY KEY (deal_id),
    FOREIGN KEY (inst_id) REFERENCES inst (inst_id),
    FOREIGN KEY (cpty_id) REFERENCES cpty (cpty_id)
) ENGINE = INNODB;
