import mysql.connector
from mysql.connector import InterfaceError


class DAO:
    def __init__(self, host, name, user, password):
        self.connector = mysql.connector.connect(
            host=host,
            database=name,
            user=user,
            password=password
        )
        self.db = self.connector.cursor()

    def reconnect_if_not_connected(self):
        if not self.connector.is_connected():
            try:
                self.connector.reconnect()
            except InterfaceError as e:
                print("Failed to reconnect.", e.msg)

    def is_connected(self):
        self.reconnect_if_not_connected()
        return self.connector.is_connected()

    def query(self, query, params=()):
        self.reconnect_if_not_connected()
        self.db.execute(query, params)
        query_result = self.db.fetchall()
        self.connector.commit()
        return query_result

    def insert_values(self, sql_insert_tuple):
        self.reconnect_if_not_connected()
        sql_insert_query = ("INSERT INTO deals (inst_id, cpty_id, price, deal_type, quantity, deal_time) VALUES"
                            "((SELECT inst_id from inst where inst_name = %s),"
                            " (SELECT cpty_id from cpty where cpty_name = %s),"
                            " %s, %s, %s, %s)")
        self.db.execute(sql_insert_query, sql_insert_tuple)
        self.connector.commit()

    def find_user(self, username):
        self.reconnect_if_not_connected()
        li = self.query("SELECT * FROM user WHERE username=%s", (username,))
        return li[0] if li else None

    def get_all_table_data(self, table_name):
        self.reconnect_if_not_connected()
        return self.query("SELECT * FROM " + table_name)

    def calculate_realised_profit(self):
        self.reconnect_if_not_connected()
        statistics_query = ("select * from("
                            "   select cpty_id, sum(total_by_deal_type) as 'realised_profit' from ("
                            "       select cpty_id, IF(deal_type='B', -sum(total), sum(total)) as 'total_by_deal_type'"
                            "       from ("
                            "           select cpty_id, deal_type, price, quantity, price * quantity as 'total'"
                            "           from deals t1"
                            "       ) t2 group by cpty_id, deal_type"
                            "   ) t3 group by cpty_id"
                            ") t4 inner join cpty on t4.cpty_id = cpty.cpty_id")
        return [(cpty, profit) for (id1, profit, id2, cpty) in self.query(statistics_query)]
