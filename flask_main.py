from flask import Flask, request, Response, jsonify
from configparser import ConfigParser
from dao import DAO
import json

from utils import Utils

config = ConfigParser()
config.read("./config.ini")

server_config = config["server"]
database_config = config["database"]

dao = DAO(database_config["host"], "flow_data", database_config["user"], database_config["password"])

app = Flask(__name__)


@app.route("/")
def hello():
    return "Hello World!"


@app.route("/data_stream", methods=['POST'])
def data_stream():
    json_obj = json.loads(request.json)
    datetime_obj = Utils.str_to_timestamp(json_obj["time"])
    dao.insert_values((json_obj["instrumentName"],
                       json_obj["cpty"],
                       json_obj["price"],
                       json_obj["type"],
                       json_obj["quantity"],
                       datetime_obj))
    return Response(status=200)


@app.route("/db_status")
def db_status():
    return str(dao.is_connected())


@app.route("/find_user_password")
def find_user_password():
    json_obj = json.loads(request.json)
    result = dao.find_user(json_obj["username"])
    return result[2] if result else ""


@app.route("/statistics/pnl_realised")
def calculate_pnl_realised():
    query_data = dao.calculate_realised_profit()
    queried_json_dict = Utils.tuples_to_json_dicts_array(query_data, ["cpty", "realised_profit"])
    return jsonify(queried_json_dict)


def boot_app():
    app.run(port=server_config["port"], threaded=True, host=server_config["host"])
