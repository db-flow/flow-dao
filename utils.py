from datetime import datetime


class Utils:
    @staticmethod
    def tuple_to_json_dict(t, keys):
        return {key: value for (key, value) in zip(keys, t)}

    @staticmethod
    def tuples_to_json_dicts_array(li, keys):
        return [Utils.tuple_to_json_dict(t, keys) for t in li]

    @staticmethod
    def str_to_timestamp(s):
        return datetime.strptime(s, "%d-%b-%Y (%H:%M:%S.%f)")

    @staticmethod
    def timestamp_to_str(timestamp):
        return timestamp.strftime("%Y-%m-%d %H:%M:%S.%f")
