import unittest
from utils import Utils


class TestFlaskMain(unittest.TestCase):
    def test_tuple_to_json(self):
        keys = ["first_name", "last_name", "id"]
        t = ("Someone", "Something", 0)

        self.assertEqual(Utils.tuple_to_json_dict(t, keys),
                         {"first_name": "Someone", "last_name": "Something", "id": 0})

    def test_tuples_to_json_dicts_array(self):
        keys = ["first_name", "last_name", "id"]
        t = [("Pavel", "Zhukov", 0),
             ("Ruslana", "Tymchyk", 1),
             ("Diana", "Nigmetzianova", 2)]

        self.assertEqual(Utils.tuples_to_json_dicts_array(t, keys),
                         [{"first_name": "Pavel", "last_name": "Zhukov", "id": 0},
                          {"first_name": "Ruslana", "last_name": "Tymchyk", "id": 1},
                          {"first_name": "Diana", "last_name": "Nigmetzianova", "id": 2}])

    def test_str_to_timestamp(self):
        result = Utils.str_to_timestamp("12-Aug-2019 (16:46:18.372409)")
        self.assertEqual(result.day, 12)
        self.assertEqual(result.month, 8)
        self.assertEqual(result.year, 2019)
        self.assertEqual(result.hour, 16)
        self.assertEqual(result.minute, 46)
        self.assertEqual(result.second, 18)
        self.assertEqual(result.microsecond, 372409)

    def test_timestamp_to_str(self):
        str_timestamp = "12-Aug-2019 (16:46:18.372409)"
        expected_result = "2019-08-12 16:46:18.372409"
        result = Utils.timestamp_to_str(Utils.str_to_timestamp(str_timestamp))
        self.assertEqual(result, expected_result)
